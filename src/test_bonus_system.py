from random import randint
from math import isclose

import pytest

from bonus_system import calculateBonuses


def random_in_bounds(a: int, b: int):
    return randint(a + 1, b - 1)


def test_none_program():
    assert calculateBonuses(None, 1337) == 0


def test_invalid_program():
    assert calculateBonuses("Invalid? sam takoy", 1337) == 0


@pytest.mark.parametrize(
    "amount,expected_result", (
        (random_in_bounds(-10000, 10000), 0.5),
        (10000, 0.75),
        (random_in_bounds(10000, 50000), 0.75),
        (50000, 1),
        (random_in_bounds(50000, 100000), 1),
        (100000, 1.25),
        (random_in_bounds(100000, 200000), 1.25),
    )
)
def test_standard_program(amount, expected_result):
    assert isclose(calculateBonuses("Standard", amount), expected_result)


@pytest.mark.parametrize(
    "amount,expected_result", (
        (random_in_bounds(-10000, 10000), 0.1),
        (10000, 0.15),
        (random_in_bounds(10000, 50000), 0.15),
        (50000, 0.2),
        (random_in_bounds(50000, 100000), 0.2),
        (100000, 0.25),
        (random_in_bounds(100000, 200000), 0.25),
    )
)
def test_premium_program(amount, expected_result):
    assert isclose(calculateBonuses("Premium", amount), expected_result)


@pytest.mark.parametrize(
    "amount,expected_result", (
        (random_in_bounds(-10000, 10000), 0.2),
        (10000, 0.3),
        (random_in_bounds(10000, 50000), 0.3),
        (50000, 0.4),
        (random_in_bounds(50000, 100000), 0.4),
        (100000, 0.5),
        (random_in_bounds(100000, 200000), 0.5),
    )
)
def test_diamond_program(amount, expected_result):
    assert isclose(calculateBonuses("Diamond", amount), expected_result)